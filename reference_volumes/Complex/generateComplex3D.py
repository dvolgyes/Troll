#!/usr/bin/python
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/

import scipy
import scipy.misc
import numpy as np

print("Converting complex2D.png to complex_256x256x256.dat...")

x=scipy.misc.imread("complex2D.png").astype(np.float32)
x=x*(255.0/np.max(x))

y=np.zeros( (256,256,256), dtype=np.float32)

for i in range(int(x.shape[0]/4),int(x.shape[0]/4*3)):
    y[i,:,:]=x
y.tofile("complex_256x256x256.dat")
