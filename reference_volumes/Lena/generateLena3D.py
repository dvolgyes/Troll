#!/usr/bin/python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License version 3 for more details.
#
#    You should have received a copy of the GNU General Public License version 3
#    along with this program (LICENSE.txt).  If not, see http://www.gnu.org/licenses/

import scipy
import scipy.misc
import numpy as np

x=np.fromfile("Lena2D_512.dat", dtype=np.float32).reshape(512,512)
x2=scipy.misc.imresize(x,0.5)
x4=scipy.misc.imresize(x,0.25)

print("Converting Lena2D to Lena3D...")

# 512x512x512 seems to be overkill, but you can try it
#y=np.zeros( (x.shape[0],x.shape[0],x.shape[0]), dtype=np.float32)
#for i in range(0,x.shape[0]):
#    angle=(float(i)-x.shape[0]/2.0)/x.shape[0]*360.0+180
#    y[i,:,:]=scipy.misc.imrotate(x,angle)
#y.tofile("Lena3D_512.dat")

y2=np.zeros( (x2.shape[0],x2.shape[0],x2.shape[0]), dtype=np.float32)
for i in range(0,x2.shape[0]):
    angle=(float(i)-x2.shape[0]/2.0)/x2.shape[0]*360.0+180
    y2[i,:,:]=scipy.misc.imrotate(x2,angle)
y2.tofile("Lena3D_256.dat")

y4=np.zeros( (x4.shape[0],x4.shape[0],x4.shape[0]), dtype=np.float32)
for i in range(0,x4.shape[0]):
    angle=(float(i)-x4.shape[0]/2.0)/x4.shape[0]*360.0+180
    y4[i,:,:]=scipy.misc.imrotate(x4,angle)
y4.tofile("Lena3D_128.dat")
