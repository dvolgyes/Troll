inline __device__ float3 operator+(const float3& a, const float3& b) 
{
    return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __device__ float3 operator-(const float3& a, const float3& b) 
{
    return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __device__ float3 operator*(const float3& a, const float& b) 
{
    return make_float3(a.x * b, a.y * b, a.z * b);
}

inline __device__ float3 operator/(const float3& a, const float& b) 
{
    return make_float3(a.x / b, a.y / b, a.z / b);
}

inline __device__ float dot(const float3& a, const float3& b)        
{
    return a.x * b.x + a.y * b.y + a.z * b.z ;
}

inline __device__ float3 cross_product(const float3& a,const float3& b)
{
    return make_float3( a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x );
}

inline __device__ float triple_product(const float3& a,const float3& b,const float3& c)
{
    return dot(a, cross_product(b, c) );
}

inline __device__ int  clamp(const int& m,const int& v) 
{
    return min(m, max(0, v) ); 
}

//     Voxel indexing scheme, could/should be texture mapping 
inline __device__ int voxelIndex(const float3& pos)  
{
    return ( clamp({{volume_dimensions[0]}} - 1, (int)pos.x)
           + clamp({{volume_dimensions[1]}} - 1, (int)pos.y) * {{volume_dimensions[0]}}
           + clamp({{volume_dimensions[2]}} - 1, (int)pos.z) * {{volume_dimensions[0]}} * {{volume_dimensions[1]}} );
}

//     The line-plane intersection problem is well known.
//     See http://en.wikipedia.org/wiki/Line-plane_intersection

inline __device__ int planeIntersection( const float3& point, const float3& direction, const float3& plane_normal, const float3& plane_point, float3& sectionPoint, float& distance)
{
    const double denominator = dot(plane_normal,direction);
    if (denominator == 0.0f)
    {
        sectionPoint = make_float3(0.0f,0.0f,0.0f);
        distance = 0.0f;
        return 0;
    }
    const double numerator = dot( (point - plane_point) , plane_normal );
    distance = -numerator / denominator;
    sectionPoint = point + direction * distance;
    return 1;
}

//     The line-box intersection is based on the line-plane intersection problem.
//     You can see the problem as a special case of the line-bounding box intersection problem.
//     http://en.wikipedia.org/wiki/Bounding_volume
//     You can read about ray-tracing:
//     https://en.wikipedia.org/wiki/Ray_tracing_%28graphics%29
//
//     and about implementations:
//     http://www.realtimerendering.com/intersections.html

__device__ bool isectionPET(const float ax0,const float ax1,const float tr0,const float tr1,float3* P0)
{
    const float3 module0=      { {{detector_pair_{{NR}}_module0_origin_str}}  };
    const float3 module1=      { {{detector_pair_{{NR}}_module1_origin_str}}  };
    const float3 axvector0=    { {{detector_pair_{{NR}}_module0_axial_str}}   };
    const float3 axvector1=    { {{detector_pair_{{NR}}_module1_axial_str}}   };
    const float3 transvector0= { {{detector_pair_{{NR}}_module0_trans_str}}   };
    const float3 transvector1= { {{detector_pair_{{NR}}_module1_trans_str}}   };
    const float3 axGapvector0=    { {{detector_pair_{{NR}}_module0_axial_gap_str}}  };
    const float3 axGapvector1=    { {{detector_pair_{{NR}}_module1_axial_gap_str}}  };
    const float3 transGapvector0= { {{detector_pair_{{NR}}_module1_trans_gap_str}}  };
    const float3 transGapvector1= { {{detector_pair_{{NR}}_module1_trans_gap_str}}  };
    
    const float3 corner1= {  {{volume_center[0]}} -0.5 * {{volume_dimensions[0]}} * {{volume_voxelsize}},
                             {{volume_center[1]}} -0.5 * {{volume_dimensions[1]}} * {{volume_voxelsize}},
                             {{volume_center[2]}} -0.5 * {{volume_dimensions[2]}} * {{volume_voxelsize}}};

    const float3 corner2 = make_float3({{volume_dimensions_str }}) ;
  
    const float3 zero = make_float3(0.0f, 0.0f, 0.0f);
    
    const float3 p0 = (module0 + axvector0 * (ax0) + axGapvector0 * (int)(ax0) + transvector0 * (tr0) + transGapvector0 * (int)(tr0) - corner1) / {{volume_voxelsize}};
    const float3 p1 = (module1 + axvector1 * (ax1) + axGapvector1 * (int)(ax1) + transvector1 * (tr1) + transGapvector1 * (int)(tr1) - corner1) / {{volume_voxelsize}};
    const float3 direction = p1 - p0;

    float3 section[6];
    float distance[6],resdis[6];
    resdis[0]=1e19; // +inf, but I need finite math
    resdis[1]=1e19;
    resdis[2]=1e19;
    resdis[3]=1e19;
    resdis[4]=1e19;
    resdis[5]=1e19;

    int s=0,counter=0;

    s+=(planeIntersection(p0,direction,make_float3(1.0f,0.0f,0.0f),zero,section[s],distance[s]));
    s+=(planeIntersection(p0,direction,make_float3(0.0f,1.0f,0.0f),zero,section[s],distance[s]));
    s+=(planeIntersection(p0,direction,make_float3(0.0f,0.0f,1.0f),zero,section[s],distance[s]));
    s+=(planeIntersection(p0,direction,make_float3(1.0f,0.0f,0.0f),corner2,section[s],distance[s]));
    s+=(planeIntersection(p0,direction,make_float3(0.0f,1.0f,0.0f),corner2,section[s],distance[s]));
    s+=(planeIntersection(p0,direction,make_float3(0.0f,0.0f,1.0f),corner2,section[s],distance[s]));

    for(int i=0; i<s; ++i)
    {
        if ( (section[i].x<zero.x || section[i].y<zero.y || section[i].z<zero.z || section[i].x>corner2.x || section[i].y>corner2.y || section[i].z>corner2.z) || ((counter==1) && fabs(distance[i]-resdis[0])<1E-4f) )
            continue;

        P0[counter]=section[i];
        resdis[counter]=distance[i];
        ++counter;
        if (counter == 2) return true;
    }
    return counter == 2;
}

//     The implementation of the forward projection is based on the publication #1, see at the end of the file.
//     The backprojection uses the same idea as the forward projection.
//     For the efficient implementation the backprojector uses 'atomicAdd instruction'.

__global__ void forwardProject
        (
        float* c_lors,
        const float* voxels,
        const float* randoms
//        ,const char  *mask
        )
{
    const int UniqueBlockIndex = blockIdx.y * gridDim.x + blockIdx.x;
    const int UniqueThreadIndex = UniqueBlockIndex * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + threadIdx.x + {{THREAD_OFFSET}};

    if (UniqueThreadIndex >= {{THREAD_OFFSET}} + {{TOTAL_THREADS}} ) return;
//    if (mask[UniqueThreadIndex]) return;
    
    for(int ray = 0; ray < {{RAYS}}; ++ray)
    {
        const int c1 = UniqueThreadIndex / ( {{detector_pair_{{NR}}_lor_shape[0]}} * {{detector_pair_{{NR}}_lor_shape[1]}} );
        const int c2 = UniqueThreadIndex % ( {{detector_pair_{{NR}}_lor_shape[2]}} * {{detector_pair_{{NR}}_lor_shape[3]}} );
        const int offsetidx = (UniqueThreadIndex + {{general_random_offset}} *ray ) % {{RNDTOTAL}} ;
        const int offsetidx0a = (offsetidx+1) % ( {{RNDTOTAL}} );
        const int offsetidx0t = (offsetidx+2) % ( {{RNDTOTAL}} );
        const int offsetidx1a = (offsetidx+3) % ( {{RNDTOTAL}} );
        const int offsetidx1t = (offsetidx+4) % ( {{RNDTOTAL}} );
        const float ax0 = c1 / {{detector_pair_{{NR}}_lor_shape[1]}} + randoms[offsetidx0a];
        const float tr0 = c1 % {{detector_pair_{{NR}}_lor_shape[1]}} + randoms[offsetidx0t];
        const float ax1 = c2 / {{detector_pair_{{NR}}_lor_shape[3]}} + randoms[offsetidx1a];
        const float tr1 = c2 % {{detector_pair_{{NR}}_lor_shape[3]}} + randoms[offsetidx1t];

        float3 P0[2];
        if (isectionPET(ax0, ax1, tr0, tr1, P0) == false ) continue;

        const float offset = randoms[offsetidx];
        const float3 direction = (P0[1] - P0[0]) / float( {{SAMPLES}} );
        const float3 start = P0[0] + direction * offset;
        const float norm = sqrt( dot(direction, direction) );

        if (norm <= 0.0f ) continue;
        
        const float3 axvector0 =    { {{detector_pair_{{NR}}_module0_axial_str}}   };
        const float3 axvector1 =    { {{detector_pair_{{NR}}_module1_axial_str}}   };
        const float3 transvector0 = { {{detector_pair_{{NR}}_module0_trans_str}}   };
        const float3 transvector1 = { {{detector_pair_{{NR}}_module1_trans_str}}   };
        
        const float weight = fabs(triple_product(direction, axvector0, transvector0) * triple_product(direction, axvector1, transvector1)) / norm ;
        double sum = 0.0;

        for(int i = 0; i < {{ SAMPLES }} ; ++i)
        {
            const int index = voxelIndex( start + direction * i);
            sum += voxels[index];
        }
        
        c_lors[UniqueThreadIndex] += sum * weight;
    }
}

__global__ void backProject(
         float* result_A, 
         float* result_B,
         const float* c_lors,
         const float* m_lors,
         const float* sens,
//         const float* isens,         
         const float* randoms
//         const char* mask 
         )
{
    const int UniqueBlockIndex = blockIdx.y * gridDim.x + blockIdx.x;
    const int UniqueThreadIndex = UniqueBlockIndex * blockDim.y * blockDim.x + (threadIdx.y * blockDim.x + threadIdx.x) +  {{THREAD_OFFSET}};

    if (UniqueThreadIndex >= {{THREAD_OFFSET}} + {{TOTAL_THREADS}} ) return;
//    if (mask[UniqueThreadIndex]) return;
    if ( c_lors[UniqueThreadIndex] <= 0.0f ) return;  // backproject only for real LORs

    for(int ray = 0; ray < {{RAYS}}; ++ray)
    {
        const int c1 = UniqueThreadIndex / ( {{detector_pair_{{NR}}_lor_shape[0]}} * {{detector_pair_{{NR}}_lor_shape[1]}} );
        const int c2 = UniqueThreadIndex % ( {{detector_pair_{{NR}}_lor_shape[2]}} * {{detector_pair_{{NR}}_lor_shape[3]}} );

        const int offsetidx = (UniqueThreadIndex + {{general_random_offset}} * ray ) % {{RNDTOTAL}} ;
        const int offsetidx0a = (offsetidx + 1 )% (  {{RNDTOTAL}} );
        const int offsetidx0t = (offsetidx + 2 )% (  {{RNDTOTAL}} );
        const int offsetidx1a = (offsetidx + 3 )% (  {{RNDTOTAL}} );
        const int offsetidx1t = (offsetidx + 4 )% (  {{RNDTOTAL}} );
        
        const float ax0 = c1 / {{detector_pair_{{NR}}_lor_shape[1]}} + randoms[offsetidx0a];
        const float tr0 = c1 % {{detector_pair_{{NR}}_lor_shape[1]}} + randoms[offsetidx0t];
        const float ax1 = c2 / {{detector_pair_{{NR}}_lor_shape[3]}} + randoms[offsetidx1a];
        const float tr1 = c2 % {{detector_pair_{{NR}}_lor_shape[3]}} + randoms[offsetidx1t];

        float3 P0[2];
        
        if (isectionPET(ax0, ax1, tr0, tr1, P0) == false) continue;

        const float offset = randoms[offsetidx];
        const float3 direction = (P0[1] - P0[0]) / float( {{SAMPLES}} );
        
        const float norm = sqrt( dot(direction, direction) );
        if ( norm <= 0.0f ) continue;
        
        const float3 axvector0 =    { {{detector_pair_{{NR}}_module0_axial_str}} };
        const float3 axvector1 =    { {{detector_pair_{{NR}}_module1_axial_str}} };
        const float3 transvector0 = { {{detector_pair_{{NR}}_module0_trans_str}} };
        const float3 transvector1 = { {{detector_pair_{{NR}}_module1_trans_str}} };
        
        const float weight = fabs(triple_product(direction, axvector0, transvector0) * triple_product(direction, axvector1, transvector1)) / norm ;
        const float ratioA = weight * ( m_lors[UniqueThreadIndex] / c_lors[UniqueThreadIndex] );
        const float ratioB = weight * sens[UniqueThreadIndex];

        const float3 start = P0[0] + direction * offset;

        for(int i = 0; i < {{ SAMPLES }}; ++i)
        {
            const int idx=voxelIndex( start + direction * i);
            atomicAdd(result_A + idx, ratioA);
            atomicAdd(result_B + idx, ratioB);
        }
    }
}

